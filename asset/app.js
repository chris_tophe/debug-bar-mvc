import './app/scss/app.scss';

const debug_btn = document.querySelector('#debug .debug_logo');
const debug_bar = document.querySelector('#debug .debug_bar');


debug_btn.addEventListener('click', function (){
    if (!debug_bar.classList.contains('open')){
        debug_bar.classList.add('open');
        setCookie("debugBarState", "open", 1);
    } else {
        debug_bar.classList.remove('open');
        setCookie("debugBarState", "close", 1);
    }
});

function setCookie(name, value, time) {
    let expires = "";
    if (time) {
        let date = new Date();
        date.setTime(date.getTime() + (time * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    let cookie_name = name + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i].trim();
        if (c.includes(cookie_name)) {
            return c.slice(cookie_name.length);
        }
    }
    return null;
}

let debugBarState = getCookie("debugBarState");

if (debugBarState === "open") {
    debug_bar.classList.add("open");
} else {
    debug_bar.classList.remove("open");
}