# DEBUG BAR

Pas besoin de dépendances externes.

Les commandes qui permettent d'afficher la debug bar sont affichées dans la fonction render de l'abstract controller dans Core/Kernel/AbstractController.php

```php
protected function render(string $viewer, array $variable = [], string  $layout = 'base')
    {
        $view = new View();
        $pro = new Profiler();
        $profiler = $pro->viewProfiler();

        ob_start();
        extract($variable);
        require $this->getViewPath().str_replace('.','/',$viewer).'.php';
        $content = ob_get_clean();
        require $this->getViewPath().'layout/'.$layout.'.php';
        die();
    }
```

Pour pouvoir déterminer si on est en développement ou non, on place une variable que l'on va utiliser dans l'objet. Dans config/config.php

```php
return array(
    'db_name'   => 'nfs_mvc_test',
    'db_user'   => 'root',
    'db_pass'   => '',
    'db_host'   => 'localhost',

    'version' => '1.0.0',

    'env' => 'dev', // dev, prod
);
```

Le controller qui permet de générer la debug bar est dans Core/Kernel/Profiler.php

```php
namespace Core\Kernel;

class Profiler extends Router
{
    protected $env;

    public function __construct()
    {
        $this->env = (new Config())->get('env');
    }

    public function viewProfiler()
    {
        $exe_time = round(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 4);

        if ($this->env === 'dev') {
            return '
            <div id="debug">
                <div class="debug_logo">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M38.8 5.1C28.4-3.1 13.3-1.2 5.1 9.2S-1.2 34.7 9.2 42.9l592 464c10.4 8.2 25.5 6.3 33.7-4.1s6.3-25.5-4.1-33.7L477.4 348.9c1.7-9.4 2.6-19 2.6-28.9h64c17.7 0 32-14.3 32-32s-14.3-32-32-32H479.7c-1.1-14.1-5-27.5-11.1-39.5c.7-.6 1.4-1.2 2.1-1.9l64-64c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-64 64c-.7 .7-1.3 1.4-1.9 2.1C409.2 164.1 393.1 160 376 160H264c-8.3 0-16.3 1-24 2.8L38.8 5.1zM320 0c-53 0-96 43-96 96v3.6c0 15.7 12.7 28.4 28.4 28.4H387.6c15.7 0 28.4-12.7 28.4-28.4V96c0-53-43-96-96-96zM160.3 256H96c-17.7 0-32 14.3-32 32s14.3 32 32 32h64c0 24.6 5.5 47.8 15.4 68.6c-2.2 1.3-4.2 2.9-6 4.8l-64 64c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l63.1-63.1c24.5 21.8 55.8 36.2 90.3 39.6V335.5L166.7 227.3c-3.4 9-5.6 18.7-6.4 28.7zM336 479.2c36.6-3.6 69.7-19.6 94.8-43.8L336 360.7V479.2z"/></svg>
                </div>

                <div class="debug_bar">
                    Votre requête a pris : ' . $exe_time . ' millisecondes à s\'exécuter ' . '<br>' . '
                    Je suis sur le controller : ' . $this->pickController() . ' ' . '
                </div>
            </div>';
        } else {
            return '';
        }
    }

    public function pickController()
    {
        include(__DIR__ . '/../../config/routes.php');

        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parse = parse_url($actual_link);
        $path = trim($parse['path'], '/');

        foreach ($routes as $key => $value){
            if ($path === ''){
                $path = 'home';
            } if (in_array($path, $value)){
                return ucfirst($value[1]).'Controller'. ' avec la méthode : '.$value[2];
            }
        }
    }
}
```

Il y a du scss pour styliser la debug bar. Dans asset/scss/app.scss.

```php
#debug{
  display: flex;
  position: relative;

  .debug_logo {
    display: block;
    width: 60px;
    height: 60px;
    background: #79C1C8;
    position: fixed;
    bottom: 0;
    right: 0;
    left: 0;
    z-index: 5;
    border: 2px solid #E8E61A;
    cursor: pointer;

    svg{
      width: 40px;
      height: 40px;
      fill: #E8E61A;
      margin: .5rem;
      z-index: 10;
    }
  }

  .debug_bar {
    display: none;
    height: 60px;
    position: fixed;
    left: 0;
    bottom: 0;
    right: 0;
    background: #79C1C8;
    color: black;
    text-align: center;
    border: 1px solid #E8E61A;

    &.open {
      display: block;
    }
  }
}
```
Du js pour pouvoir faire en sorte que la barre sorte ou se rétracte quand on appuie sur le bouton et aussi pour qu'elle reste ouverte si on décide de changer de page. Dans asset/app.js

```php
import './app/scss/app.scss';

const debug_btn = document.querySelector('#debug .debug_logo');
const debug_bar = document.querySelector('#debug .debug_bar');


debug_btn.addEventListener('click', function (){
    if (!debug_bar.classList.contains('open')){
        debug_bar.classList.add('open');
        setCookie("debugBarState", "open", 1);
    } else {
        debug_bar.classList.remove('open');
        setCookie("debugBarState", "close", 1);
    }
});

function setCookie(name, value, time) {
    let expires = "";
    if (time) {
        let date = new Date();
        date.setTime(date.getTime() + (time * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    let cookie_name = name + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i].trim();
        if (c.includes(cookie_name)) {
            return c.slice(cookie_name.length);
        }
    }
    return null;
}

let debugBarState = getCookie("debugBarState");

if (debugBarState === "open") {
    debug_bar.classList.add("open");
} else {
    debug_bar.classList.remove("open");
}
```