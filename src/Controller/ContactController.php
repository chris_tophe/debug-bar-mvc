<?php

namespace App\Controller;

use Core\Kernel\AbstractController;
use App\Service\Form;
use App\Service\Validation;

class ContactController extends AbstractController{

    public function index(){
        $errors = array();

        if (!empty($_POST['sent'])){
            $post = $this->cleanXss($_POST);

            $validate = new Validation();
            $errors['email'] = $validate->emailValid($post['email']);
            $errors['sujet'] = $validate->textValid($post['sujet'], 'sujet', 2, 20);
            $errors['message'] = $validate->textValid($post['message'], 'message', 5, 200);

            if ($validate->IsValid($errors)){

                $this->addFlash('success', 'Merci de votre message');
                $this->redirect('home');
            }
        }

        $form = new Form($errors);
        $this->render('app.default.contact', array(
            'form' => $form,
        ));
    }
}