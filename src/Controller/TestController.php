<?php

namespace App\Controller;


use Core\Kernel\AbstractController;

class TestController extends AbstractController{

    public function index_test(){
        $title = 'Bienvenue sur la page de confirmation';

        $this->render('app.default.test', array(
            'title' => $title,
        ));
    }
}