<div class="wrap">
    <h2>Page contact</h2>

    <form action="" method="post" novalidate id="formulaire">
        <?php
        echo $form->label('email');
        echo $form->input('email', 'email');
        echo $form->error('email');

        echo $form->label('sujet');
        echo $form->input('sujet', 'text');
        echo $form->error('sujet');

        echo $form->label('message');
        echo $form->textarea('message');
        echo $form->error('message');

        echo $form->submit('sent', 'Envoyer');
        ?>
    </form>
</div>